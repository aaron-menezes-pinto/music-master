import React, { Component } from 'react';
import Search from './Search';
import Artist from './Artist';
import Tracks from './Tracks';

const API_ADDRESS = 'https://spotify-api-wrapper.appspot.com'

class App extends Component {
    state = { artist: null, tracks: [] };

    componentDidMount() {
        this.searchArtist('tupac');
    }
    searchArtist = artistQuery => {
        fetch(`${API_ADDRESS}/artist/${artistQuery}`)
        .then(response => {
            if(response.ok){
                return response.json();
            } else{
                throw new Error('Network response was not ok.');
            }
        })
        .then(json =>{
            if(json.artists.total > 0){
                const artist = json.artists.items[0];
                this.setState({ artist });

                fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
                .then(response => {
                    if(response.ok){
                        return response.json();
                    } else{
                        throw new Error('Network response was not ok.');
                    }
                })
                .then(json => {
                    this.setState({ tracks: json.tracks });
                })
                .catch(error => {
                    console.log('There was an error retrieving top tracks for '
                        + 'the artist on spotify', 
                        error.message);
                });
            }
        })
        .catch(error => {
            console.log('There was an error searching for the artist on spotify', 
                error.message);
        });
    }

    render() {
        return (
            <div>
                <h2>Music Master</h2>
                <Search searchArtist={ this.searchArtist } />
                <Artist artist={ this.state.artist } />
                <Tracks tracks={ this.state.tracks } />
            </div>
        );
    }
}

export default App;
